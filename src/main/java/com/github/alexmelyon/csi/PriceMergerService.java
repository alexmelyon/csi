package com.github.alexmelyon.csi;

import java.util.List;

public interface PriceMergerService {

    public List<PriceItem> merge(List<PriceItem> src, List<PriceItem> dst);
}
