package com.github.alexmelyon.csi;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Date;
import java.util.TimeZone;

public class PriceItem {

    public long id;
    public String product_code;
    int number;
    int depart;
    Date begin;
    Date end;
    long value;

    public PriceItem() {
    }

    public PriceItem(String product_code, int number, int depart, Date begin, Date end, long value) {
        this.product_code = product_code;
        this.number = number;
        this.depart = depart;
        this.begin = begin;
        this.end = end;
        this.value = value;
    }

    public PriceItem(String product_code, int number, int depart, String begin, String end, long value) throws ParseException {
        this.product_code = product_code;
        this.number = number;
        this.depart = depart;
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        this.begin = df.parse(begin);
        this.end = df.parse(end);
        this.value = value;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getDepart() {
        return depart;
    }

    public void setDepart(int depart) {
        this.depart = depart;
    }

    public Date getBegin() {
        return begin;
    }

    public void setBegin(Date begin) {
        this.begin = begin;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public String toString() {
        return this.getClass().getSimpleName()
                + "(id = " + String.format("%10d", hashCode())
                + ", product_code = " + product_code
                + ", number = " + number
                + ", depart = " + depart
                + ", begin = " + begin
                + ", end = " + end
                + ", value = " + value + ")";
    }
}
