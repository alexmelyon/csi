package com.github.alexmelyon.csi;

import java.util.*;
import java.util.stream.Collectors;

public class PriceMergerServiceImpl implements PriceMergerService {

    @Override
    public List<PriceItem> merge(List<PriceItem> src, List<PriceItem> dst) {
        List<PriceItem> both = new ArrayList<>(src);
        both.addAll(dst);

        List<PriceItem> res = new ArrayList<>();

        // Find unique product_code, number, depart and process it separately
        both.stream().map(it -> it.getProduct_code()).distinct().forEach(product_code -> {
            both.stream().map(it -> it.getNumber()).distinct().forEach(number -> {
                both.stream().map(it -> it.getDepart()).distinct().forEach(depart -> {
                    List<PriceItem> oneSrc = src.stream().filter(it ->
                        it.getProduct_code().equals(product_code) && it.getNumber() == number && it.getDepart() == depart
                    ).collect(Collectors.toList());

                    List<PriceItem> oneDst = dst.stream().filter(it ->
                        it.getProduct_code().equals(product_code) && it.getNumber() == number && it.getDepart() == depart
                    ).collect(Collectors.toList());

                    List<PriceItem> oneRes = mergeIdentical(oneSrc, oneDst, product_code, number, depart);
                    res.addAll(oneRes);
                });
            });
        });
        return res;
    }

    protected List<PriceItem> mergeIdentical(List<PriceItem> src, List<PriceItem> dst, String product_code, int number, int depart) {

        checkIdenticalFields(src, dst, product_code, number, depart);

        if (src.isEmpty()) {
            return dst;
        } else if (dst.isEmpty()) {
            return src;
        }

        // Find continuous sequences and cut from source
        List<PricesSequence> sequences = new ArrayList<>();
        dst.stream().sorted(Comparator.comparing(a -> a.begin)).forEach(newPrice -> findSequences(newPrice, sequences));
        List<PriceItem> cutted = src.stream()
                .filter(source -> sequences.stream().anyMatch(seqItem -> source.begin.after(seqItem.begin) && source.getEnd().before(seqItem.getEnd())))
                .collect(Collectors.toList());

        List<PriceItem> remaining = src.stream().filter(it -> !cutted.contains(it)).collect(Collectors.toList());

        // Find intersecting in source prices and replace it
        List<PriceItem> res = remaining.stream().map(it -> {
            Optional<PriceItem> dstIntersects = dst.stream().filter(dstItem -> isIntersectsAny(it, dstItem)).findFirst();
            if (dstIntersects.isPresent()) {
                return mergeTwo(it, dstIntersects.get());
            } else {
                return Arrays.asList(it);
            }
        }).flatMap(it -> it.stream()).distinct().collect(Collectors.toList());

        // Add missing not intersecting destination prices
        List<PriceItem> noIntersects = dst.stream()
                .filter(dstItem -> remaining.stream().allMatch(srcItem -> !isIntersectsAny(srcItem, dstItem)))
                .collect(Collectors.toList());
        res.addAll(noIntersects);
        //
        return res;
    }

    protected List<PriceItem> mergeTwo(PriceItem oldPrice, PriceItem newPrice) {
        List<PriceItem> res = new ArrayList<>();
        res.add(newPrice);

        if (isReplacement(oldPrice, newPrice)) {
            // do nothing

        } else if (isExpand(oldPrice, newPrice)) {
            oldPrice.setEnd((Date) newPrice.getEnd().clone());
            res.clear();
            res.add(oldPrice);

        } else if (isOverlap(oldPrice, newPrice)) {
            // do nothing

        } else if (isIntersectsBegin(oldPrice, newPrice)) {
            res.add(new PriceItem(oldPrice.getProduct_code(), oldPrice.getNumber(), oldPrice.getDepart(), newPrice.getEnd(), oldPrice.getEnd(), oldPrice.getValue()));

        } else if (isIntersectsEnd(oldPrice, newPrice)) {
            res.add(new PriceItem(oldPrice.getProduct_code(), oldPrice.getNumber(), oldPrice.getDepart(), oldPrice.getBegin(), newPrice.getBegin(), oldPrice.getValue()));

        } else if (isInclude(oldPrice, newPrice)) {
            res.add(new PriceItem(oldPrice.getProduct_code(), oldPrice.getNumber(), oldPrice.getDepart(), oldPrice.getBegin(), newPrice.getBegin(), oldPrice.getValue()));
            res.add(new PriceItem(oldPrice.getProduct_code(), oldPrice.getNumber(), oldPrice.getDepart(), newPrice.getEnd(), oldPrice.getEnd(), oldPrice.getValue()));

        } else {
            res.add(oldPrice);
        }
        return res;
    }

    private void checkIdenticalFields(List<PriceItem> src, List<PriceItem> dst, String product_code, int number, int depart) {
        List<PriceItem> srcDst = new ArrayList<>(src);
        srcDst.addAll(dst);
        if (srcDst.stream().anyMatch(it -> !it.getProduct_code().equals(product_code))) {
            String wrongCode = srcDst.stream().filter(it -> !it.getProduct_code().equals(product_code)).findFirst().get().getProduct_code();
            throw new WrongPriceItemFieldRuntimeException("product_code: " + wrongCode + " must match with " + product_code);
        }
        if (srcDst.stream().anyMatch(it -> it.getNumber() != number)) {
            int wrongNumber = srcDst.stream().filter(it -> it.getNumber() != number).findFirst().get().getNumber();
            throw new WrongPriceItemFieldRuntimeException("number: " + wrongNumber + " must match with " + number);
        }
        if (srcDst.stream().anyMatch(it -> it.getDepart() != depart)) {
            int wrongDepart = srcDst.stream().filter(it -> it.getDepart() != depart).findFirst().get().getDepart();
            throw new WrongPriceItemFieldRuntimeException("depart: " + wrongDepart + " must match with " + depart);
        }
    }

    private boolean isIntersectsAny(PriceItem oldPrice, PriceItem newPrice) {
        return isInclude(oldPrice, newPrice) || isIntersectsBegin(oldPrice, newPrice) || isIntersectsEnd(oldPrice, newPrice);
    }

    private void findSequences(PriceItem newPrice, List<PricesSequence> sequences) {
        Optional<PricesSequence> first = sequences.stream().filter(it -> it.getEnd().equals(newPrice.getBegin())).findFirst();
        if (first.isPresent()) {
            first.get().add(newPrice);
        } else {
            sequences.add(new PricesSequence(newPrice));
        }
    }

    private boolean isInclude(PriceItem oldPrice, PriceItem newPrice) {
        return newPrice.getBegin().after(oldPrice.getBegin()) && newPrice.getEnd().before(oldPrice.getEnd());
    }

    private boolean isIntersectsEnd(PriceItem oldPrice, PriceItem newPrice) {
        return isBetween(newPrice.getBegin(), oldPrice.getBegin(), oldPrice.getEnd()) && (newPrice.getEnd().after(oldPrice.getEnd()) || newPrice.getEnd().equals(oldPrice.getEnd()));
    }

    private boolean isIntersectsBegin(PriceItem oldPrice, PriceItem newPrice) {
        return (newPrice.getBegin().before(oldPrice.getBegin()) || newPrice.getBegin().equals(oldPrice.getBegin())) && isBetween(newPrice.getEnd(), oldPrice.getBegin(), oldPrice.getEnd());
    }

    private boolean isOverlap(PriceItem oldPrice, PriceItem newPrice) {
        return newPrice.getBegin().before(oldPrice.getBegin()) && newPrice.getEnd().after(oldPrice.getEnd());
    }

    private boolean isExpand(PriceItem oldPrice, PriceItem newPrice) {
        return newPrice.getBegin().after(oldPrice.begin) && newPrice.begin.before(oldPrice.getEnd()) && newPrice.getEnd().after(oldPrice.getEnd()) && newPrice.getValue() == oldPrice.value;
    }

    private boolean isReplacement(PriceItem oldPrice, PriceItem newPrice) {
        return newPrice.getBegin().equals(oldPrice.getBegin()) && newPrice.getEnd().equals(oldPrice.getEnd());
    }

    private boolean isBetween(Date date, Date begin, Date end) {
        return date.after(begin) && date.before(end);
    }
}
