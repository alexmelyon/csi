package com.github.alexmelyon.csi;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PricesSequence {
    List<PriceItem> items = new ArrayList<>();

    Date begin;
    Date end;

    public PricesSequence(PriceItem item) {
        this.items.add(item);
        this.begin = item.begin;
        this.end = item.end;
    }

    public void add(PriceItem item) {
        this.items.add(item);
        this.end = item.end;
    }

    public Date getBegin() {
        return begin;
    }

    public void setBegin(Date begin) {
        this.begin = begin;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }
}
