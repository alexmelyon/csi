package com.github.alexmelyon.csi;

public class WrongPriceItemFieldRuntimeException extends RuntimeException {

    public WrongPriceItemFieldRuntimeException(String product_code) {
        super(product_code);
    }
}
