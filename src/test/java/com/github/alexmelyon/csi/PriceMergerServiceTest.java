package com.github.alexmelyon.csi;

import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class PriceMergerServiceTest {

    private static PriceMergerFakeServiceImpl priceMergerService;

    List<PriceItem> src;
    List<PriceItem> dst;

    @BeforeMethod
    public void setUp() {
        src = new ArrayList<>();
        dst = new ArrayList<>();
        priceMergerService = Mockito.spy(new PriceMergerFakeServiceImpl());
    }

    @Test
    public void shouldReturnEmpty() {
        List<PriceItem> res = priceMergerService.merge(src, dst);
        Assert.assertTrue(res.isEmpty());
    }

    @Test
    public void shouldInclude_ex1() throws ParseException {
        src.add(new PriceItem("", 0, 0, "01.01.2017 00:00:00", "31.01.2017 00:00:00", 50L));
        dst.add(new PriceItem("", 0, 0, "02.01.2017 00:00:00", "03.01.2017 00:00:00", 60L));
        List<PriceItem> res = priceMergerService.merge(src, dst);
        List<PriceItem> sorted = res.stream().sorted(Comparator.comparing(a -> a.begin)).collect(Collectors.toList());

        Mockito.verify(priceMergerService).mergeIdentical(Mockito.anyList(), Mockito.anyList(), Mockito.anyString(), Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(priceMergerService).mergeTwo(Mockito.anyObject(), Mockito.anyObject());

        Assert.assertTrue(sorted.get(0).getValue() == 50L);
        Assert.assertTrue(dateEquals(sorted.get(0).begin, "01.01.2017 00:00:00"));
        Assert.assertTrue(dateEquals(sorted.get(0).end, "02.01.2017 00:00:00"));

        Assert.assertTrue(sorted.get(1).getValue() == 60L);
        Assert.assertTrue(dateEquals(sorted.get(1).begin, "02.01.2017 00:00:00"));
        Assert.assertTrue(dateEquals(sorted.get(1).end, "03.01.2017 00:00:00"));

        Assert.assertTrue(sorted.get(2).getValue() == 50L);
        Assert.assertTrue(dateEquals(sorted.get(2).begin, "03.01.2017 00:00:00"));
        Assert.assertTrue(dateEquals(sorted.get(2).end, "31.01.2017 00:00:00"));
    }

    @Test
    public void shouldIntersects_ex2() throws ParseException {
        src.add(new PriceItem("", 0, 0, "01.01.2017 00:00:00", "03.01.2017 00:00:00", 100L));
        src.add(new PriceItem("", 0, 0, "03.01.2017 00:00:00", "05.01.2017 00:00:00", 120L));

        dst.add(new PriceItem("", 0, 0, "02.01.2017 00:00:00", "04.01.2017 00:00:00", 110L));

        List<PriceItem> res = priceMergerService.merge(src, dst);
        List<PriceItem> sorted = res.stream().sorted(Comparator.comparing(a -> a.begin)).collect(Collectors.toList());

        Mockito.verify(priceMergerService).mergeIdentical(Mockito.anyList(), Mockito.anyList(), Mockito.anyString(), Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(priceMergerService, Mockito.times(2)).mergeTwo(Mockito.anyObject(), Mockito.anyObject());

        Assert.assertTrue(sorted.get(0).getValue() == 100);
        Assert.assertTrue(dateEquals(sorted.get(0).begin, "01.01.2017 00:00:00"));
        Assert.assertTrue(dateEquals(sorted.get(0).end, "02.01.2017 00:00:00"));

        Assert.assertTrue(sorted.get(1).getValue() == 110);
        Assert.assertTrue(dateEquals(sorted.get(1).begin, "02.01.2017 00:00:00"));
        Assert.assertTrue(dateEquals(sorted.get(1).end, "04.01.2017 00:00:00"));

        Assert.assertTrue(sorted.get(2).getValue() == 120);
        Assert.assertTrue(dateEquals(sorted.get(2).begin, "04.01.2017 00:00:00"));
        Assert.assertTrue(dateEquals(sorted.get(2).end, "05.01.2017 00:00:00"));
    }

    @Test
    public void shouldCut_ex3() throws ParseException {
        src.add(new PriceItem("", 0, 0, "01.01.2017 00:00:00", "03.01.2017 00:00:00", 80L));
        src.add(new PriceItem("", 0, 0, "03.01.2017 00:00:00", "05.01.2017 00:00:00", 87L));
        src.add(new PriceItem("", 0, 0, "05.01.2017 00:00:00", "07.01.2017 00:00:00", 90L));

        dst.add(new PriceItem("", 0, 0, "02.01.2017 00:00:00", "04.01.2017 00:00:00", 80L));
        dst.add(new PriceItem("", 0, 0, "04.01.2017 00:00:00", "06.01.2017 00:00:00", 85L));

        List<PriceItem> res = priceMergerService.merge(src, dst);
        List<PriceItem> sorted = res.stream().sorted(Comparator.comparing(a -> a.begin)).collect(Collectors.toList());

        Mockito.verify(priceMergerService).mergeIdentical(Mockito.anyList(), Mockito.anyList(), Mockito.anyString(), Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(priceMergerService, Mockito.times(2)).mergeTwo(Mockito.anyObject(), Mockito.anyObject());

        Assert.assertTrue(sorted.get(0).getValue() == 80L);
        Assert.assertTrue(dateEquals(sorted.get(0).begin, "01.01.2017 00:00:00"));
        Assert.assertTrue(dateEquals(sorted.get(0).end, "04.01.2017 00:00:00"));

        Assert.assertTrue(sorted.get(1).getValue() == 85L);
        Assert.assertTrue(dateEquals(sorted.get(1).begin, "04.01.2017 00:00:00"));
        Assert.assertTrue(dateEquals(sorted.get(1).end, "06.01.2017 00:00:00"));

        Assert.assertTrue(sorted.get(2).getValue() == 90L);
        Assert.assertTrue(dateEquals(sorted.get(2).begin, "06.01.2017 00:00:00"));
        Assert.assertTrue(dateEquals(sorted.get(2).end, "07.01.2017 00:00:00"));
    }

    @Test
    public void shouldGetBoth() throws ParseException {
        src.add(new PriceItem("", 0, 0, "01.01.2017 00:00:00", "02.01.2017 00:00:00", 1L));
        dst.add(new PriceItem("", 0, 0, "03.01.2017 00:00:00", "04.01.2017 00:00:00", 2L));
        List<PriceItem> res = priceMergerService.merge(src, dst);
        Assert.assertTrue(res.size() == 2);
    }

    @Test
    public void shouldGetSource() throws ParseException {
        src.add(new PriceItem("", 0, 0, "01.01.2017 00:00:00", "02.01.2017 00:00:00", 1L));
        List<PriceItem> res = priceMergerService.merge(src, dst);
        Assert.assertTrue(res.size() == 1);
    }

    @Test
    public void shouldGetDest() throws ParseException {
        dst.add(new PriceItem("", 0, 0, "01.01.2017 00:00:00", "02.01.2017 00:00:00", 1L));
        List<PriceItem> res = priceMergerService.merge(src, dst);
        Assert.assertTrue(res.size() == 1);
    }

    private boolean dateEquals(Date date, String other) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        Date otherDate = df.parse(other);
        return date.equals(otherDate);
    }
}