package com.github.alexmelyon.csi;

import com.github.alexmelyon.csi.PriceItem;
import com.github.alexmelyon.csi.PriceMergerServiceImpl;

import java.util.List;

public class PriceMergerFakeServiceImpl extends PriceMergerServiceImpl {

    public List<PriceItem> mergeTwo(PriceItem oldPrice, PriceItem newPrice) {
        return super.mergeTwo(oldPrice, newPrice);
    }

    public List<PriceItem> mergeIdentical(List<PriceItem> src, List<PriceItem> dst, String product_code, int number, int depart) {
        return super.mergeIdentical(src, dst, product_code, number, depart);
    }
}
